import React from "react";
import "./App.css";
import { capitalize } from "./utils";

export interface StatData {
  count: number;
  likes: number;
}

interface StatDetailProps {
  breed: string;
  statData: StatData;
}

export const StatDetail: React.FC<StatDetailProps> = ({
  breed,
  statData: { count, likes },
}) => {
  return (
    <div className="stat-detail">
      <h6>{capitalize(breed)}</h6>
      <p>Count: {count}</p>
      <p>Likes: {likes}</p>
    </div>
  );
};
