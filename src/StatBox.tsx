import React, { useMemo } from "react";
import "./App.css";
import { StatData, StatDetail } from "./StatDetail";

interface StatBoxProps {
  stat: { [key: string]: StatData };
}

export const StatBox: React.FC<StatBoxProps> = ({ stat }) => {
  const statkeys = useMemo(() => Object.keys(stat), [stat]);

  return (
    <div className="stat-box">
      {statkeys.map((statkey, index) => (
        <StatDetail
          key={`stat-detail-${index}`}
          breed={statkey}
          statData={stat[statkey]}
        />
      ))}
    </div>
  );
};
