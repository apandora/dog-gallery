import React, { useContext, useEffect, useState } from "react";
import { AppContext } from "./App";
import "./App.css";

interface GalleryCellProps {
  index: number;
  breed: string;
  likes: number;
}

export const GalleryCell: React.FC<GalleryCellProps> = ({
  index,
  breed,
  likes = 0,
}) => {
  const [imgSrc, setImgSrc] = useState<string | null>(null);
  const { addLike } = useContext(AppContext);

  useEffect(() => {
    fetch(`https://dog.ceo/api/breed/${breed}/images/random`)
      .then((response) => response.json())
      .then((data) => {
        const { message: url } = data;
        setImgSrc(url);
      });
  }, [breed]);

  return (
    <div className="gallery-cell" onClick={() => addLike(breed, index)}>
      <h6>{breed}</h6>
      {imgSrc && <img src={imgSrc} alt={breed} title={breed} />}
      <p>Likes: {likes}</p>
    </div>
  );
};
