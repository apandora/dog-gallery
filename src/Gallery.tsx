import React from "react";
import "./App.css";
import { GalleryCell } from "./GalleryCell";

interface GalleryProps {
  breeds: [string, number][];
}

export const Gallery: React.FC<GalleryProps> = ({ breeds = [] }) => {
  return (
    <div className="gallery-box">
      {breeds.map(([breed, likes], index) => (
        <GalleryCell
          key={`random-dog-${index}`}
          index={index}
          breed={breed}
          likes={likes}
        />
      ))}
    </div>
  );
};
