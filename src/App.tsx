import { useEffect, useState, useCallback, createContext } from "react";
import "./App.css";
import { Gallery } from "./Gallery";
import { StatBox } from "./StatBox";
import { StatData } from "./StatDetail";

const GALLERY_SIZE = 100;

interface AppContextInterface {
  addLike: (breed: string, index: number) => void;
}

export const AppContext = createContext<AppContextInterface>(
  {} as AppContextInterface
);

function App() {
  const [breedList, setBreedList] = useState<string[]>([]);
  const [galleryBreeds, setGalleryBreeds] = useState<[string, number][]>([]);
  const [statData, setStatData] = useState<{ [key: string]: StatData }>({});

  const handleAddLike = useCallback(
    (breed: string, index: number) => {
      if (!galleryBreeds) {
        return;
      }

      const newGalleryBreeds = [...galleryBreeds];
      const [, currLikes] = galleryBreeds[index];
      newGalleryBreeds[index] = [breed, currLikes + 1];
      setGalleryBreeds(newGalleryBreeds);

      const newStatData = { ...statData };
      const { count, likes: currTotalLikes } = statData[breed];
      newStatData[breed] = { count, likes: currTotalLikes + 1 };
      setStatData(newStatData);
    },
    [galleryBreeds, statData]
  );

  const initializeGalleryBreeds = useCallback(
    (breeds: string[]): [[string, number][], { [key: string]: StatData }] => {
      if (!breeds) {
        return [[], {}];
      }

      const gallery: [string, number][] = [...new Array(GALLERY_SIZE)].map(
        () => {
          const index = Math.floor(Math.random() * (breeds.length + 1));
          return [breeds[index] ?? breeds[0], 0];
        }
      );

      const breedCount = gallery.reduce(
        (obj: { [key: string]: number }, item) => {
          const [breed] = item;
          obj[breed] = obj[breed] ? ++obj[breed] : 1;
          return obj;
        },
        {}
      );

      const stat: { [key: string]: StatData } = {};

      for (const breed of Object.keys(breedCount)) {
        stat[breed] = {
          count: breedCount[breed] ?? 0,
          likes: 0,
        } as StatData;
      }

      return [gallery, stat];
    },
    []
  );

  useEffect(() => {
    fetch("https://dog.ceo/api/breeds/list/all")
      .then((response) => response.json())
      .then((data) => {
        const { message } = data;
        const breeds = Object.keys(message);
        setBreedList(breeds);
        const [gallery, stat] = initializeGalleryBreeds(breeds);
        setStatData(stat);
        setGalleryBreeds(gallery);
      });
  }, [initializeGalleryBreeds]);

  return (
    <AppContext.Provider
      value={{
        addLike: (breed: string, index: number) => handleAddLike(breed, index),
      }}
    >
      <div className="App">
        {breedList && (
          <>
            <div className="left-side">
              <StatBox stat={statData} />
            </div>
            <div className="right-side">
              <Gallery breeds={galleryBreeds} />
            </div>
          </>
        )}
      </div>
    </AppContext.Provider>
  );
}

export default App;
